#  SPDX-License-Identifier: GPL-2.0-or-later
#
#  This file is part of plymouth.
#
#  plymouth is free software; you can redistribute it and/or modify it
#  under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.

[Unit]
Description=Graphical Getty on %I
Documentation=man:plymouth(8) man:systemd-getty-generator(8)
Documentation=http://0pointer.de/blog/projects/serial-console.html
After=systemd-user-sessions.service plymouth-quit-wait.service getty-pre.target
After=rc-local.service

# If additional gettys are spawned during boot then we should make
# sure that this is synchronized before getty.target, even though
# getty.target didn't actually pull it in.
Before=getty.target
IgnoreOnIsolate=yes

# IgnoreOnIsolate causes issues with sulogin, if someone isolates
# rescue.target or starts rescue.service from multi-user.target or
# graphical.target.
Conflicts=rescue.service
Before=rescue.service

# On systems without virtual consoles, don't start any getty. Note
# that serial gettys are covered by serial-getty@.service, not this
# unit.
ConditionPathExists=/dev/tty0

[Service]
# the VT is cleared by TTYVTDisallocate
ExecCondition=-@PLYMOUTH_CLIENT_DIR@/plymouth quit
ExecCondition=-@PLYMOUTH_CLIENT_DIR@/plymouth --wait
ExecCondition=@PLYMOUTH_DAEMON_DIR@/plymouthd --pid-file=@plymouthruntimedir@/pid --tty=%I --no-boot-log
ExecStartPre=@PLYMOUTH_CLIENT_DIR@/plymouth show-splash
ExecStart=sh -c '\
    if LOGNAME=`plymouth ask-question --prompt="$HOSTNAME login on %I"`; then\
        plymouth quit;\
        plymouth --wait;\
        exec agetty -no "-p -- $LOGNAME" --noclear %I $TERM;\
    fi;\
    exit 1\
'
Type=idle
Restart=on-success
RestartSec=0
UtmpIdentifier=%I
TTYPath=/dev/%I
TTYReset=yes
TTYVHangup=yes
TTYVTDisallocate=yes
IgnoreSIGPIPE=no
SendSIGHUP=yes

# Unset locale for the console getty since the console has problems
# displaying some internationalized messages.
UnsetEnvironment=LANG LANGUAGE LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT LC_IDENTIFICATION

[Install]
WantedBy=getty.target
DefaultInstance=tty1
